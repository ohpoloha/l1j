package l1j.server.server;

import java.text.DecimalFormat;

import l1j.server.server.command.executor.L1Brave;
import l1j.server.server.command.executor.L1CDK;
import l1j.server.server.command.executor.L1PlayerBuff;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_SystemMessage;

public class PlayerCommands {
	private static PlayerCommands _instance;

	public static PlayerCommands getInstance() {
		if (_instance == null) {
			_instance = new PlayerCommands();
		}
		return _instance;
	}

	public boolean handleCommands(L1PcInstance pc, String cmd) {

		if (cmd.equals("buff")) {
			L1PlayerBuff.getInstance().execute(pc, cmd, null);
			return true;
		} else if (cmd.equals("brave")) {
			L1Brave.getInstance().execute(pc, cmd, null);
			return true;
		} else if (cmd.equals("cdk")) {
			L1CDK.getInstance().execute(pc, cmd, null);
			return true;
		} else if (cmd.equals("jobs")) {
		
			String strJobs = getMinerExp(pc) + getFisherExp(pc)
					+ getFarmerExp(pc) + getButcherExp(pc) + getLumberExp(pc)
					+ getHerbalistExp(pc);

			pc.sendPackets(new S_SystemMessage("\\f=" + strJobs));
			return true;
		}

		return false;
	}
	
	private String getMinerExp(L1PcInstance pc) {
		if (pc.get_Miner() == -1) return "";
		DecimalFormat df = new DecimalFormat("##.####");
		int lvl = (int) pc.get_Miner() / 10000;
		double percent = ((double) pc.get_Miner() / 10000) - lvl;
		String strMiner = "Miner: Lv" + lvl + " " + df.format(percent * 100) + "%\n";
		return strMiner;
	}
	private String getFisherExp(L1PcInstance pc) {
		if (pc.get_Fisher() == -1) return "";
		DecimalFormat df = new DecimalFormat("##.####");
		int lvl = (int) pc.get_Fisher() / 10000;
		double percent = ((double) pc.get_Fisher() / 10000) - lvl;
		String strFisher = "Fisher: Lv" + lvl + " " + df.format(percent * 100) + "%\n";
		return strFisher;
	}
	private String getFarmerExp(L1PcInstance pc) {
		if (pc.get_Farmer() == -1) return "";
		DecimalFormat df = new DecimalFormat("##.####");
		int lvl = (int) pc.get_Farmer() / 10000;
		double percent = ((double) pc.get_Farmer() / 10000) - lvl;
		String strFarmer = "Farmer: Lv" + lvl + " " + df.format(percent * 100) + "%\n";
		return strFarmer;
	}
	private String getButcherExp(L1PcInstance pc) {
		if (pc.get_Butcher() == -1) return "";
		DecimalFormat df = new DecimalFormat("##.####");
		int lvl = (int) pc.get_Butcher() / 10000;
		double percent = ((double) pc.get_Butcher() / 10000) - lvl;
		String strButcher = "Butcher: Lv" + lvl + " " + df.format(percent * 100) + "%\n";
		return strButcher;
	}
	private String getLumberExp(L1PcInstance pc) {
		if (pc.get_Lumber() == -1) return "";
		DecimalFormat df = new DecimalFormat("##.####");
		int lvl = (int) pc.get_Lumber() / 10000;
		double percent = ((double) pc.get_Lumber() / 10000) - lvl;
		String strLumber = "Lumber: Lv" + lvl + " " + df.format(percent * 100) + "%\n";
		return strLumber;
	}
	private String getHerbalistExp(L1PcInstance pc) {
		if (pc.get_Herbalist() == -1) return "";
		DecimalFormat df = new DecimalFormat("##.####");
		int lvl = (int) pc.get_Herbalist() / 10000;
		double percent = ((double) pc.get_Herbalist() / 10000) - lvl;
		String strHerbalist = "Herbalist: Lv" + lvl + " " + df.format(percent * 100) + "%\n";
		return strHerbalist;
	}
}
