package l1j.server.server.model.Instance;

import java.util.ArrayList;

import l1j.server.Config;
import l1j.server.server.model.L1PolyMorph;
import l1j.server.server.model.item.L1JobButcherItems;
import l1j.server.server.model.item.L1JobFarmerItems;
import l1j.server.server.model.item.L1JobFisherItems;
import l1j.server.server.model.item.L1JobHerbalistItems;
import l1j.server.server.model.item.L1JobLumberItems;
import l1j.server.server.model.item.L1JobMinerItems;
import l1j.server.server.serverpackets.S_DoActionGFX;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.Random;

public class L1JobInstance {

	L1PcInstance pc;
	int jobLvl, jobId;
	
	public static final int MINER = 0;
	public static final int FISHER = 1;
	public static final int FARMER = 2;
	public static final int BUTCHER = 3;
	public static final int LUMBER = 4;
	public static final int HERBALIST = 5;
	
	public L1JobInstance(L1PcInstance pc, int jobId) {
		this.pc = pc;
		this.jobId = jobId;
	}
	
	public boolean isSuccess() {
		int rnd = Random.nextInt(100);
		int successRate = 0;

		if (jobLvl >= 90) {
			successRate = 90;
		} else {
			successRate = jobLvl;
		}

		if (successRate >= rnd) {
			return true;
		}

		return false;
	}
	
	public void addExp (L1PcInstance pc, int job) {
			
		switch (job) {
		case MINER:
			jobLvl = (int) pc.get_Miner() / 10000; // 總共100等
			if (jobLvl != 100) {
				int exp = (100 - jobLvl) * 10;
				if (pc.get_Miner() + (exp * Config.Rate_JobExp) >= 1000000)
					pc.set_Miner(999999);
				else
					pc.set_Miner(pc.get_Miner() + (exp * Config.Rate_JobExp));
			}
			break;
		case FISHER:
			if (jobLvl != 100) {
				jobLvl = (int) pc.get_Fisher() / 10000; // 總共100等
				int exp = (100 - jobLvl) * 10;
				if (pc.get_Fisher() + (exp * Config.Rate_JobExp) >= 1000000)
					pc.set_Fisher(999999);
				else
					pc.set_Fisher(pc.get_Fisher() + (exp * Config.Rate_JobExp));
			}
			break;
		case FARMER:
			if (jobLvl != 100) {
				jobLvl = (int) pc.get_Farmer() / 10000; // 總共100等
				int exp = (100 - jobLvl) * 10;
				if (pc.get_Farmer() + (exp * Config.Rate_JobExp) >= 1000000)
					pc.set_Farmer(999999);
				else
					pc.set_Farmer(pc.get_Farmer() + (exp * Config.Rate_JobExp));
			}
			break;
		case BUTCHER:
			jobLvl = (int) pc.get_Butcher() / 10000; // 總共100等
			if (jobLvl != 100) {
				int exp = (100 - jobLvl) * 10;
				if (pc.get_Butcher() + (exp * Config.Rate_JobExp) >= 1000000)
					pc.set_Butcher(999999);
				else
					pc.set_Butcher(pc.get_Butcher() + (exp * Config.Rate_JobExp));
			}
			break;
		case LUMBER:
			jobLvl = (int) pc.get_Lumber() / 10000; // 總共100等
			if (jobLvl != 100) {
				int exp = (100 - jobLvl) * 10;
				if (pc.get_Lumber() + (exp * Config.Rate_JobExp) >= 1000000)
					pc.set_Lumber(999999);
				else
					pc.set_Lumber(pc.get_Lumber() + (exp * Config.Rate_JobExp));
			}
			break;
		case HERBALIST:
			jobLvl = (int) pc.get_Herbalist() / 10000; // 總共100等
			if (jobLvl != 100) {
				int exp = (100 - jobLvl) * 10;
				if (pc.get_Herbalist() + (exp * Config.Rate_JobExp) >= 1000000)
					pc.set_Herbalist(999999);
				else
					pc.set_Herbalist(pc.get_Herbalist() + (exp * Config.Rate_JobExp));
			}
			break;

		}
	}
	/**
	 * 用來組成String
	 * job + "已經開始嚕..."
	 * @param job
	 * @throws InterruptedException
	 */
	public void startJob (String job) throws InterruptedException {
		pc.sendPackets(new S_SystemMessage("\\fW" + job + "已經開始嚕..."));
		L1PolyMorph.doPoly(pc, 3642, 5, L1PolyMorph.MORPH_BY_GM); // 變成歐姆礦工人
		pc.sendPackets(new S_DoActionGFX(pc.getId(), 3)); // 工人挖礦的動作
		Thread.sleep(6000); // 閒置時間
		addExp(pc, jobId);
	}
	
	public int getRandomItem() {
		
		int itemId;
		ArrayList alBasic = new ArrayList();
		ArrayList alSilver = new ArrayList();
		ArrayList alGold = new ArrayList();
		ArrayList alPremiun = new ArrayList();
		ArrayList alDiamond = new ArrayList();

		switch (jobId) {
		case MINER:
			 alBasic = L1JobMinerItems.basic;
			 alSilver = L1JobMinerItems.silver;
			 alGold = L1JobMinerItems.gold;
			 alPremiun = L1JobMinerItems.premium;
			 alDiamond = L1JobMinerItems.diamond;
			 break;
		case FISHER:
			 alBasic = L1JobFisherItems.basic;
			 alSilver = L1JobFisherItems.silver;
			 alGold = L1JobFisherItems.gold;
			 alPremiun = L1JobFisherItems.premium;
			 alDiamond = L1JobFisherItems.diamond;
			break;
		case FARMER:
			 alBasic = L1JobFarmerItems.basic;
			 alSilver = L1JobFarmerItems.silver;
			 alGold = L1JobFarmerItems.gold;
			 alPremiun = L1JobFarmerItems.premium;
			 alDiamond = L1JobFarmerItems.diamond;
			break;
		case BUTCHER:
			 alBasic = L1JobButcherItems.basic;
			 alSilver = L1JobButcherItems.silver;
			 alGold = L1JobButcherItems.gold;
			 alPremiun = L1JobButcherItems.premium;
			 alDiamond = L1JobButcherItems.diamond;
			break;
		case LUMBER:
			 alBasic = L1JobLumberItems.basic;
			 alSilver = L1JobLumberItems.silver;
			 alGold = L1JobLumberItems.gold;
			 alPremiun = L1JobLumberItems.premium;
			 alDiamond = L1JobLumberItems.diamond;
			break;
		case HERBALIST:
			 alBasic = L1JobHerbalistItems.basic;
			 alSilver = L1JobHerbalistItems.silver;
			 alGold = L1JobHerbalistItems.gold;
			 alPremiun = L1JobHerbalistItems.premium;
			 alDiamond = L1JobHerbalistItems.diamond;
			break;
		}
				
		int b = Random.nextInt(alBasic.size());
		int s = Random.nextInt(alSilver.size());
		int g = Random.nextInt(alGold.size());
		int p = Random.nextInt(alPremiun.size());
		int d = Random.nextInt(alDiamond.size());

		int range = 100 + (jobLvl / 10);
		int rnd = Random.nextInt(range);
		int r1 = (byte) Random.nextInt(6) + 1; // 隨機(0~5) + 額外		
		
		if (rnd == 109 && jobLvl == 100) {
			itemId = (int) alDiamond.get(d);
			pc.sendPackets(new S_SystemMessage("\\fW似乎挖到非常稀有的物品了。"));		
		} else if (rnd >= 105 && jobLvl >= 90) {
			itemId = (int) alPremiun.get(p);
			pc.sendPackets(new S_SystemMessage("\\fW似乎挖到高級的物品了。"));
		} else if (rnd >= 90 && jobLvl >= 75) {
			itemId = (int) alGold.get(g);
			pc.sendPackets(new S_SystemMessage("\\fW似乎挖到不錯的物品了。"));
		} else if (rnd >= 55 && jobLvl >= 55) {
			itemId = (int) alSilver.get(s);
			pc.sendPackets(new S_SystemMessage("\\fW似乎挖到物品了。"));
		} else if (rnd >= 30 ) {
			itemId = (int) alBasic.get(b);
			pc.sendPackets(new S_SystemMessage("\\fW似乎挖到很普通的物品。"));
		} else {
			itemId = 0;
			pc.sendPackets(new S_SystemMessage("\\fW什麼都沒有挖到。"));
		} 

		return itemId;
	}
}
