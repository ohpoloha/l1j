package l1j.server.server.model.item;

import java.util.ArrayList;
import java.util.Arrays;

import l1j.server.Config;

public class L1JobMinerItems {
	public static ArrayList basic = new ArrayList (Arrays.asList( Config.MINER_b1, Config.MINER_b2, Config.MINER_b3, Config.MINER_b4, Config.MINER_b5 ));// 一般
	public static ArrayList silver = new ArrayList (Arrays.asList( Config.MINER_s1, Config.MINER_s2, Config.MINER_s3, Config.MINER_s4 ));// 中等
	public static ArrayList gold = new ArrayList (Arrays.asList( Config.MINER_g1, Config.MINER_g2, Config.MINER_g3, Config.MINER_g4 ));// 高等
	public static ArrayList premium = new ArrayList (Arrays.asList( Config.MINER_p1, Config.MINER_p2, Config.MINER_p3, Config.MINER_p4 ));// 鑽石
	public static ArrayList diamond = new ArrayList (Arrays.asList( Config.MINER_d1, Config.MINER_d2, Config.MINER_d3, Config.MINER_d4, Config.MINER_d5 ));// 黃金
}
