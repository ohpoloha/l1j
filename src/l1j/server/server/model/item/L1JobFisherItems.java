package l1j.server.server.model.item;

import java.util.ArrayList;
import java.util.Arrays;

import l1j.server.Config;

public class L1JobFisherItems {
	public static ArrayList basic = new ArrayList (Arrays.asList( Config.FISHER_b1, Config.FISHER_b2, Config.FISHER_b3, Config.FISHER_b4, Config.FISHER_b5 ));// 一般
	public static ArrayList silver = new ArrayList (Arrays.asList( Config.FISHER_s1, Config.FISHER_s2, Config.FISHER_s3, Config.FISHER_s4 ));// 中等
	public static ArrayList gold = new ArrayList (Arrays.asList( Config.FISHER_g1, Config.FISHER_g2, Config.FISHER_g3, Config.FISHER_g4 ));// 高等
	public static ArrayList premium = new ArrayList (Arrays.asList( Config.FISHER_p1, Config.FISHER_p2, Config.FISHER_p3, Config.FISHER_p4 ));// 鑽石
	public static ArrayList diamond = new ArrayList (Arrays.asList( Config.FISHER_d1, Config.FISHER_d2, Config.FISHER_d3, Config.FISHER_d4, Config.FISHER_d5 ));// 黃金
}
