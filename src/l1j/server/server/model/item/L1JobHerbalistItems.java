package l1j.server.server.model.item;

import java.util.ArrayList;
import java.util.Arrays;

import l1j.server.Config;

public class L1JobHerbalistItems {
	public static ArrayList basic = new ArrayList (Arrays.asList( Config.HERBALIST_b1, Config.HERBALIST_b2, Config.HERBALIST_b3, Config.HERBALIST_b4, Config.HERBALIST_b5 ));// 一般
	public static ArrayList silver = new ArrayList (Arrays.asList( Config.HERBALIST_s1, Config.HERBALIST_s2, Config.HERBALIST_s3, Config.HERBALIST_s4 ));// 中等
	public static ArrayList gold = new ArrayList (Arrays.asList( Config.HERBALIST_g1, Config.HERBALIST_g2, Config.HERBALIST_g3, Config.HERBALIST_g4 ));// 高等
	public static ArrayList premium = new ArrayList (Arrays.asList( Config.HERBALIST_p1, Config.HERBALIST_p2, Config.HERBALIST_p3, Config.HERBALIST_p4 ));// 鑽石
	public static ArrayList diamond = new ArrayList (Arrays.asList( Config.HERBALIST_d1, Config.HERBALIST_d2, Config.HERBALIST_d3, Config.HERBALIST_d4, Config.HERBALIST_d5 ));// 黃金
}
