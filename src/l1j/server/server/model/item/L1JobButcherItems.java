package l1j.server.server.model.item;

import java.util.ArrayList;
import java.util.Arrays;

import l1j.server.Config;

public class L1JobButcherItems {
	public static ArrayList basic = new ArrayList (Arrays.asList( Config.BUTCHER_b1, Config.BUTCHER_b2, Config.BUTCHER_b3, Config.BUTCHER_b4, Config.BUTCHER_b5 ));// 一般
	public static ArrayList silver = new ArrayList (Arrays.asList( Config.BUTCHER_s1, Config.BUTCHER_s2, Config.BUTCHER_s3, Config.BUTCHER_s4 ));// 中等
	public static ArrayList gold = new ArrayList (Arrays.asList( Config.BUTCHER_g1, Config.BUTCHER_g2, Config.BUTCHER_g3, Config.BUTCHER_g4 ));// 高等
	public static ArrayList premium = new ArrayList (Arrays.asList( Config.BUTCHER_p1, Config.BUTCHER_p2, Config.BUTCHER_p3, Config.BUTCHER_p4 ));// 鑽石
	public static ArrayList diamond = new ArrayList (Arrays.asList( Config.BUTCHER_d1, Config.BUTCHER_d2, Config.BUTCHER_d3, Config.BUTCHER_d4, Config.BUTCHER_d5 ));// 黃金
}
