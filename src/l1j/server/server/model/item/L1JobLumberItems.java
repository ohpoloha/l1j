package l1j.server.server.model.item;

import java.util.ArrayList;
import java.util.Arrays;

import l1j.server.Config;

public class L1JobLumberItems {
	public static ArrayList basic = new ArrayList (Arrays.asList( Config.LUMBER_b1, Config.LUMBER_b2, Config.LUMBER_b3, Config.LUMBER_b4, Config.LUMBER_b5 ));// 一般
	public static ArrayList silver = new ArrayList (Arrays.asList( Config.LUMBER_s1, Config.LUMBER_s2, Config.LUMBER_s3, Config.LUMBER_s4 ));// 中等
	public static ArrayList gold = new ArrayList (Arrays.asList( Config.LUMBER_g1, Config.LUMBER_g2, Config.LUMBER_g3, Config.LUMBER_g4 ));// 高等
	public static ArrayList premium = new ArrayList (Arrays.asList( Config.LUMBER_p1, Config.LUMBER_p2, Config.LUMBER_p3, Config.LUMBER_p4 ));// 鑽石
	public static ArrayList diamond = new ArrayList (Arrays.asList( Config.LUMBER_d1, Config.LUMBER_d2, Config.LUMBER_d3, Config.LUMBER_d4, Config.LUMBER_d5 ));// 黃金
}
