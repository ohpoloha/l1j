package l1j.server.server.model.item;

import java.util.ArrayList;
import java.util.Arrays;

import l1j.server.Config;

public class L1JobFarmerItems {
	public static ArrayList basic = new ArrayList (Arrays.asList( Config.FARMER_b1, Config.FARMER_b2, Config.FARMER_b3, Config.FARMER_b4, Config.FARMER_b5 ));// 一般
	public static ArrayList silver = new ArrayList (Arrays.asList( Config.FARMER_s1, Config.FARMER_s2, Config.FARMER_s3, Config.FARMER_s4 ));// 中等
	public static ArrayList gold = new ArrayList (Arrays.asList( Config.FARMER_g1, Config.FARMER_g2, Config.FARMER_g3, Config.FARMER_g4 ));// 高等
	public static ArrayList premium = new ArrayList (Arrays.asList( Config.FARMER_p1, Config.FARMER_p2, Config.FARMER_p3, Config.FARMER_p4 ));// 鑽石
	public static ArrayList diamond = new ArrayList (Arrays.asList( Config.FARMER_d1, Config.FARMER_d2, Config.FARMER_d3, Config.FARMER_d4, Config.FARMER_d5 ));// 黃金
}
