/**
 *                            License
 * THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS  
 * CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). 
 * THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW.  
 * ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR  
 * COPYRIGHT LAW IS PROHIBITED.
 * 
 * BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND  
 * AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE  
 * MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED 
 * HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
 * 
 */
package l1j.server.server.command.executor;

import static l1j.server.server.model.identity.L1ItemId.ADENA;

import java.util.StringTokenizer;

import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1BuffUtil;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;

public class L1Brave implements L1CommandExecutor {
	private L1Brave() {
	}

	public static L1CommandExecutor getInstance() {
		return new L1Brave();
	}

	@Override
	public void execute(L1PcInstance pc, String cmdName, String arg) {
		
		if (pc.getLevel() > 40) {
			if (pc.getInventory().checkItem(ADENA, 10000)) {				
				pc.getInventory().consumeItem(ADENA, 10000);
			} else {
				pc.sendPackets(new S_SystemMessage("錢不足10000, 無法購買"));
				return;
			}
		} else {
			pc.sendPackets(new S_SystemMessage("40級以下免費brave"));
		}
			
		
//		int[] allBuffSkill =
//		{ LIGHT, SHIELD, DECREASE_WEIGHT, PHYSICAL_ENCHANT_DEX, PHYSICAL_ENCHANT_STR, BLESS_WEAPON, ADVANCE_SPIRIT};
		try {
			StringTokenizer st = new StringTokenizer(pc.getName());
			String name = st.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer(name);
			if (target == null) {
				pc.sendPackets(new S_ServerMessage(73, name)); // \f1%0はゲームをしていません。
				return;
			}

//			L1BuffUtil.haste(target, 3600 * 1000);
			L1BuffUtil.brave(target, 3600 * 1000);
//			L1PolyMorph.doPoly(target, 5641, 7200, L1PolyMorph.MORPH_BY_GM);
//			for (int element : allBuffSkill) {
//				L1Skills skill = SkillsTable.getInstance().getTemplate(element);
//				new L1SkillUse().handleCommands(target, element, target.getId(), target.getX(), target.getY(), null, skill.getBuffDuration() * 1000,
//						L1SkillUse.TYPE_GMBUFF);
//			}
		}
		catch (Exception e) {
//			pc.sendPackets(new S_SystemMessage("請輸入 .allBuff 玩家名稱。"));
		}
	}
}
