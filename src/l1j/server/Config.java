/**
 *                            License
 * THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS  
 * CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). 
 * THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW.  
 * ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS LICENSE OR  
 * COPYRIGHT LAW IS PROHIBITED.
 * 
 * BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND  
 * AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. TO THE EXTENT THIS LICENSE  
 * MAY BE CONSIDERED TO BE A CONTRACT, THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED 
 * HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
 * 
 */
package l1j.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.server.utils.IntRange;

public final class Config {
	private static final Logger _log = Logger.getLogger(Config.class.getName());

	// 挖礦
	// basic
	public static int MINER_b1 = 49270;
	public static int MINER_b2 = 40141;
	public static int MINER_b3 = 40145;
	public static int MINER_b4 = 40151;
	public static int MINER_b5 = 40152;

	// silver
	public static int MINER_s1 = 40044;
	public static int MINER_s2 = 40045;
	public static int MINER_s3 = 40046;
	public static int MINER_s4 = 40047;

	// gold
	public static int MINER_g1 = 40048;
	public static int MINER_g2 = 40049;
	public static int MINER_g3 = 40050;
	public static int MINER_g4 = 40051;

	// premium
	public static int MINER_p1 = 40052;
	public static int MINER_p2 = 40053;
	public static int MINER_p3 = 40054;
	public static int MINER_p4 = 40055;

	// diamond
	public static int MINER_d1 = 40524;
	public static int MINER_d2 = 40304;
	public static int MINER_d3 = 40305;
	public static int MINER_d4 = 40306;
	public static int MINER_d5 = 40307;

	// 釣魚
	// basic
	public static int FISHER_b1 = 49270;
	public static int FISHER_b2 = 40141;
	public static int FISHER_b3 = 40145;
	public static int FISHER_b4 = 40151;
	public static int FISHER_b5 = 40152;

	// silver
	public static int FISHER_s1 = 40044;
	public static int FISHER_s2 = 40045;
	public static int FISHER_s3 = 40046;
	public static int FISHER_s4 = 40047;

	// gold
	public static int FISHER_g1 = 40048;
	public static int FISHER_g2 = 40049;
	public static int FISHER_g3 = 40050;
	public static int FISHER_g4 = 40051;

	// premium
	public static int FISHER_p1 = 40052;
	public static int FISHER_p2 = 40053;
	public static int FISHER_p3 = 40054;
	public static int FISHER_p4 = 40055;

	// diamond
	public static int FISHER_d1 = 40524;
	public static int FISHER_d2 = 40304;
	public static int FISHER_d3 = 40305;
	public static int FISHER_d4 = 40306;
	public static int FISHER_d5 = 40307;

	// 農夫
	// basic
	public static int FARMER_b1 = 49270;
	public static int FARMER_b2 = 40141;
	public static int FARMER_b3 = 40145;
	public static int FARMER_b4 = 40151;
	public static int FARMER_b5 = 40152;

	// silver
	public static int FARMER_s1 = 40044;
	public static int FARMER_s2 = 40045;
	public static int FARMER_s3 = 40046;
	public static int FARMER_s4 = 40047;

	// gold
	public static int FARMER_g1 = 40048;
	public static int FARMER_g2 = 40049;
	public static int FARMER_g3 = 40050;
	public static int FARMER_g4 = 40051;

	// premium
	public static int FARMER_p1 = 40052;
	public static int FARMER_p2 = 40053;
	public static int FARMER_p3 = 40054;
	public static int FARMER_p4 = 40055;

	// diamond
	public static int FARMER_d1 = 40524;
	public static int FARMER_d2 = 40304;
	public static int FARMER_d3 = 40305;
	public static int FARMER_d4 = 40306;
	public static int FARMER_d5 = 40307;

	// 屠夫
	// basic
	public static int BUTCHER_b1 = 49270;
	public static int BUTCHER_b2 = 40141;
	public static int BUTCHER_b3 = 40145;
	public static int BUTCHER_b4 = 40151;
	public static int BUTCHER_b5 = 40152;

	// silver
	public static int BUTCHER_s1 = 40044;
	public static int BUTCHER_s2 = 40045;
	public static int BUTCHER_s3 = 40046;
	public static int BUTCHER_s4 = 40047;

	// gold
	public static int BUTCHER_g1 = 40048;
	public static int BUTCHER_g2 = 40049;
	public static int BUTCHER_g3 = 40050;
	public static int BUTCHER_g4 = 40051;

	// premium
	public static int BUTCHER_p1 = 40052;
	public static int BUTCHER_p2 = 40053;
	public static int BUTCHER_p3 = 40054;
	public static int BUTCHER_p4 = 40055;

	// diamond
	public static int BUTCHER_d1 = 40524;
	public static int BUTCHER_d2 = 40304;
	public static int BUTCHER_d3 = 40305;
	public static int BUTCHER_d4 = 40306;
	public static int BUTCHER_d5 = 40307;

	// 樵夫
	// basic
	public static int LUMBER_b1 = 49270;
	public static int LUMBER_b2 = 40141;
	public static int LUMBER_b3 = 40145;
	public static int LUMBER_b4 = 40151;
	public static int LUMBER_b5 = 40152;

	// silver
	public static int LUMBER_s1 = 40044;
	public static int LUMBER_s2 = 40045;
	public static int LUMBER_s3 = 40046;
	public static int LUMBER_s4 = 40047;

	// gold
	public static int LUMBER_g1 = 40048;
	public static int LUMBER_g2 = 40049;
	public static int LUMBER_g3 = 40050;
	public static int LUMBER_g4 = 40051;

	// premium
	public static int LUMBER_p1 = 40052;
	public static int LUMBER_p2 = 40053;
	public static int LUMBER_p3 = 40054;
	public static int LUMBER_p4 = 40055;

	// diamond
	public static int LUMBER_d1 = 40524;
	public static int LUMBER_d2 = 40304;
	public static int LUMBER_d3 = 40305;
	public static int LUMBER_d4 = 40306;
	public static int LUMBER_d5 = 40307;

	// 採藥
	// basic
	public static int HERBALIST_b1 = 49270;
	public static int HERBALIST_b2 = 40141;
	public static int HERBALIST_b3 = 40145;
	public static int HERBALIST_b4 = 40151;
	public static int HERBALIST_b5 = 40152;

	// silver
	public static int HERBALIST_s1 = 40044;
	public static int HERBALIST_s2 = 40045;
	public static int HERBALIST_s3 = 40046;
	public static int HERBALIST_s4 = 40047;

	// gold
	public static int HERBALIST_g1 = 40048;
	public static int HERBALIST_g2 = 40049;
	public static int HERBALIST_g3 = 40050;
	public static int HERBALIST_g4 = 40051;

	// premium
	public static int HERBALIST_p1 = 40052;
	public static int HERBALIST_p2 = 40053;
	public static int HERBALIST_p3 = 40054;
	public static int HERBALIST_p4 = 40055;

	// diamond
	public static int HERBALIST_d1 = 40524;
	public static int HERBALIST_d2 = 40304;
	public static int HERBALIST_d3 = 40305;
	public static int HERBALIST_d4 = 40306;
	public static int HERBALIST_d5 = 40307;

	/** Debug/release mode */
	public static final boolean DEBUG = false;

	/** Thread pools size */
	public static int THREAD_P_EFFECTS;

	public static int THREAD_P_GENERAL;

	public static int AI_MAX_THREAD;

	public static int THREAD_P_TYPE_GENERAL;

	public static int THREAD_P_SIZE_GENERAL;

	/** Server control */
	public static String GAME_SERVER_HOST_NAME;

	public static int GAME_SERVER_PORT;

	public static String DB_DRIVER;

	public static String DB_URL;

	public static String DB_LOGIN;

	public static String DB_PASSWORD;

	public static String TIME_ZONE;

	public static int CLIENT_LANGUAGE;

	public static String CLIENT_LANGUAGE_CODE;

	public static String[] LANGUAGE_CODE_ARRAY = { "UTF8", "EUCKR", "UTF8", "BIG5", "SJIS", "GBK" };

	public static boolean HOSTNAME_LOOKUPS;

	public static int AUTOMATIC_KICK;

	public static boolean AUTO_CREATE_ACCOUNTS;

	public static short MAX_ONLINE_USERS;

	public static boolean CACHE_MAP_FILES;

	public static boolean LOAD_V2_MAP_FILES;

	public static boolean CHECK_MOVE_INTERVAL;

	public static boolean CHECK_ATTACK_INTERVAL;

	public static boolean CHECK_SPELL_INTERVAL;

	public static short INJUSTICE_COUNT;

	public static int JUSTICE_COUNT;

	public static int CHECK_STRICTNESS;
	
	public static int ILLEGAL_SPEEDUP_PUNISHMENT;

	public static int AUTOSAVE_INTERVAL;

	public static int AUTOSAVE_INTERVAL_INVENTORY;

	public static int SKILLTIMER_IMPLTYPE;

	public static int NPCAI_IMPLTYPE;

	public static boolean TELNET_SERVER;

	public static int TELNET_SERVER_PORT;

	public static int PC_RECOGNIZE_RANGE;

	public static boolean CHARACTER_CONFIG_IN_SERVER_SIDE;

	public static boolean ALLOW_2PC;

	public static int LEVEL_DOWN_RANGE;

	public static boolean SEND_PACKET_BEFORE_TELEPORT;

	public static boolean DETECT_DB_RESOURCE_LEAKS;
	
	public static boolean CmdActive;
	
	public static int Announcements_Cycle_Time;
	
	public static boolean Announcements_Cycle_Modify_Time;

	/** Original Rates */
	public static int Ori_Rate_CombatExp;
	public static int Ori_Rate_JobExp;
	public static int Ori_Rate_PetExp;
	public static int Ori_Rate_DollExp;
	public static int Ori_Rate_Honor;
	public static int Ori_EleEnchant_Weapon;
	public static int Ori_EleEnchant_Armor;	
	public static int Ori_REWARD_TIME;	
	public static int Ori_REWARD_COUNT;	
	public static int Ori_REWARD_ITEM;	
	public static double Ori_RATE_XP;	
	public static double Ori_RATE_LA;
	public static double Ori_RATE_KARMA;
	public static double Ori_RATE_DROP_ADENA;
	public static double Ori_RATE_DROP_ITEMS;
	public static int Ori_ENCHANT_CHANCE_WEAPON;
	public static int Ori_ENCHANT_CHANCE_ARMOR;
	/** Rate control */

	public static int Rate_CombatExp;

	public static int Rate_JobExp;

	public static int Rate_PetExp;

	public static int Rate_DollExp;

	public static int Rate_Honor;

	public static int EleEnchant_Weapon;

	public static int EleEnchant_Armor;
	
	public static int REWARD_TIME;
	
	public static int REWARD_COUNT;
	
	public static double RATE_XP;
	
	public static double RATE_LA;

	public static double RATE_KARMA;

	public static double RATE_DROP_ADENA;

	public static double RATE_DROP_ITEMS;

	public static int ENCHANT_CHANCE_WEAPON;

	public static int ENCHANT_CHANCE_ARMOR;

	public static int ATTR_ENCHANT_CHANCE;

	public static double RATE_WEIGHT_LIMIT;

	public static double RATE_WEIGHT_LIMIT_PET;

	public static double RATE_SHOP_SELLING_PRICE;

	public static double RATE_SHOP_PURCHASING_PRICE;

	public static int CREATE_CHANCE_DIARY;

	public static int CREATE_CHANCE_RECOLLECTION;

	public static int CREATE_CHANCE_MYSTERIOUS;

	public static int CREATE_CHANCE_PROCESSING;

	public static int CREATE_CHANCE_PROCESSING_DIAMOND;

	public static int CREATE_CHANCE_DANTES;

	public static int CREATE_CHANCE_ANCIENT_AMULET;

	public static int CREATE_CHANCE_HISTORY_BOOK;

	public static int MAGIC_STONE_TYPE; // 附魔石類型

	public static int MAGIC_STONE_LEVEL; // 附魔石階級

	/** AltSettings control */

	public static boolean GUI;//管理者介面byeric1300460
	
	public static boolean ONLINE_REWARD;
	
	public static int REWARD_ITEM;
	
	public static short GLOBAL_CHAT_LEVEL;

	public static short WHISPER_CHAT_LEVEL;

	public static byte AUTO_LOOT;

	public static int LOOTING_RANGE;

	public static boolean ALT_NONPVP;

	public static boolean ALT_ATKMSG;

	public static boolean CHANGE_TITLE_BY_ONESELF;

	public static int MAX_CLAN_MEMBER;

	public static boolean CLAN_ALLIANCE;

	public static int MAX_PT;

	public static int MAX_CHAT_PT;

	public static boolean SIM_WAR_PENALTY;

	public static boolean GET_BACK;

	public static String ALT_ITEM_DELETION_TYPE;

	public static int ALT_ITEM_DELETION_TIME;

	public static int ALT_ITEM_DELETION_RANGE;

	public static boolean ALT_GMSHOP;

	public static int ALT_GMSHOP_MIN_ID;

	public static int ALT_GMSHOP_MAX_ID;

	public static boolean ALT_HALLOWEENIVENT;

	public static boolean ALT_JPPRIVILEGED;

	public static boolean ALT_TALKINGSCROLLQUEST;

	public static boolean ALT_WHO_COMMAND;

	public static boolean ALT_REVIVAL_POTION;

	public static int ALT_WAR_TIME;

	public static int ALT_WAR_TIME_UNIT;

	public static int ALT_WAR_INTERVAL;

	public static int ALT_WAR_INTERVAL_UNIT;

	public static int ALT_RATE_OF_DUTY;

	public static boolean SPAWN_HOME_POINT;

	public static int SPAWN_HOME_POINT_RANGE;

	public static int SPAWN_HOME_POINT_COUNT;

	public static int SPAWN_HOME_POINT_DELAY;

	public static boolean INIT_BOSS_SPAWN;

	public static int ELEMENTAL_STONE_AMOUNT;

	public static int HOUSE_TAX_INTERVAL;

	public static int MAX_DOLL_COUNT;

	public static boolean RETURN_TO_NATURE;

	public static int MAX_NPC_ITEM;

	public static int MAX_PERSONAL_WAREHOUSE_ITEM;

	public static int MAX_CLAN_WAREHOUSE_ITEM;

	public static boolean DELETE_CHARACTER_AFTER_7DAYS;

	public static int NPC_DELETION_TIME;

	public static int DEFAULT_CHARACTER_SLOT;
	
	public static int GDROPITEM_TIME;
	
	public static int RESTART_TIME; // 重新啟動伺服器
	public static int MAX_ABILITY;// add 能力值上限
	public static int MAX_ELIXIRS;// add 万能一共可以喝多少6种加起来
	public static int MAX_ABILITY_AFTER_ELIXIRS;// add 属性多少后不可以喝
	public static short CrackStartTime;// 時空裂痕重新開啟時間(單位小時)
	public static short CrackOpenTime;// 時空裂痕打開多久(單位分鐘)

	/** CharSettings control */
	public static int PRINCE_MAX_HP;

	public static int PRINCE_MAX_MP;

	public static int KNIGHT_MAX_HP;

	public static int KNIGHT_MAX_MP;

	public static int ELF_MAX_HP;

	public static int ELF_MAX_MP;

	public static int WIZARD_MAX_HP;

	public static int WIZARD_MAX_MP;

	public static int DARKELF_MAX_HP;

	public static int DARKELF_MAX_MP;

	public static int DRAGONKNIGHT_MAX_HP;

	public static int DRAGONKNIGHT_MAX_MP;

	public static int ILLUSIONIST_MAX_HP;

	public static int ILLUSIONIST_MAX_MP;

	public static int LV50_EXP;

	public static int LV51_EXP;

	public static int LV52_EXP;

	public static int LV53_EXP;

	public static int LV54_EXP;

	public static int LV55_EXP;

	public static int LV56_EXP;

	public static int LV57_EXP;

	public static int LV58_EXP;

	public static int LV59_EXP;

	public static int LV60_EXP;

	public static int LV61_EXP;

	public static int LV62_EXP;

	public static int LV63_EXP;

	public static int LV64_EXP;

	public static int LV65_EXP;

	public static int LV66_EXP;

	public static int LV67_EXP;

	public static int LV68_EXP;

	public static int LV69_EXP;

	public static int LV70_EXP;

	public static int LV71_EXP;

	public static int LV72_EXP;

	public static int LV73_EXP;

	public static int LV74_EXP;

	public static int LV75_EXP;

	public static int LV76_EXP;

	public static int LV77_EXP;

	public static int LV78_EXP;

	public static int LV79_EXP;

	public static int LV80_EXP;

	public static int LV81_EXP;

	public static int LV82_EXP;

	public static int LV83_EXP;

	public static int LV84_EXP;

	public static int LV85_EXP;

	public static int LV86_EXP;

	public static int LV87_EXP;

	public static int LV88_EXP;

	public static int LV89_EXP;

	public static int LV90_EXP;

	public static int LV91_EXP;

	public static int LV92_EXP;

	public static int LV93_EXP;

	public static int LV94_EXP;

	public static int LV95_EXP;

	public static int LV96_EXP;

	public static int LV97_EXP;

	public static int LV98_EXP;

	public static int LV99_EXP;
	
	public static int LV100_EXP;
	public static int LV101_EXP;
	public static int LV102_EXP;
	public static int LV103_EXP;
	public static int LV104_EXP;
	public static int LV105_EXP;
	public static int LV106_EXP;
	public static int LV107_EXP;
	public static int LV108_EXP;
	public static int LV109_EXP;
	public static int LV110_EXP;
	public static int LV111_EXP;
	public static int LV112_EXP;
	public static int LV113_EXP;
	public static int LV114_EXP;
	public static int LV115_EXP;
	public static int LV116_EXP;
	public static int LV117_EXP;
	public static int LV118_EXP;
	public static int LV119_EXP;
	public static int LV120_EXP;

	/** FightSettings control */
	public static boolean FIGHT_IS_ACTIVE;

	public static boolean NOVICE_PROTECTION_IS_ACTIVE;

	public static int NOVICE_MAX_LEVEL;

	public static int NOVICE_PROTECTION_LEVEL_RANGE;
	
	/**Record Settings*/
	public static byte LOGGING_WEAPON_ENCHANT;

	public static byte LOGGING_ARMOR_ENCHANT;

	public static boolean LOGGING_CHAT_NORMAL;

	public static boolean LOGGING_CHAT_WHISPER;

	public static boolean LOGGING_CHAT_SHOUT;

	public static boolean LOGGING_CHAT_WORLD;

	public static boolean LOGGING_CHAT_CLAN;

	public static boolean LOGGING_CHAT_PARTY;

	public static boolean LOGGING_CHAT_COMBINED;

	public static boolean LOGGING_CHAT_CHAT_PARTY;
	
	public static boolean writeTradeLog;
	
	public static boolean writeRobotsLog;
	
	public static boolean writeDropLog;
	
	public static int MysqlAutoBackup;
	
	public static boolean CompressGzip;

	/** Configuration files */
	public static final String SERVER_CONFIG_FILE = "./config/server.properties";

	public static final String RATES_CONFIG_FILE = "./config/rates.properties";

	public static final String ALT_SETTINGS_FILE = "./config/altsettings.properties";

	public static final String CHAR_SETTINGS_CONFIG_FILE = "./config/charsettings.properties";

	public static final String FIGHT_SETTINGS_CONFIG_FILE = "./config/fights.properties";
	
	public static final String RECORD_SETTINGS_CONFIG_FILE = "./config/record.properties";

	/** 其他設定 */

	// 吸收每個 NPC 的 MP 上限
	public static final int MANA_DRAIN_LIMIT_PER_NPC = 40;

	// 每一次攻擊吸收的 MP 上限(馬那、鋼鐵馬那）
	public static final int MANA_DRAIN_LIMIT_PER_SOM_ATTACK = 9;

	public static void load() {
		_log.info("loading gameserver config");
		// server.properties
		try {
			Properties serverSettings = new Properties();
			InputStream is = new FileInputStream(new File(SERVER_CONFIG_FILE));
			serverSettings.load(is);
			is.close();

			GAME_SERVER_HOST_NAME = serverSettings.getProperty("GameserverHostname", "*");
			GAME_SERVER_PORT = Integer.parseInt(serverSettings.getProperty("GameserverPort", "2000"));
			DB_DRIVER = serverSettings.getProperty("Driver", "com.mysql.jdbc.Driver");
			DB_URL = serverSettings.getProperty("URL", "jdbc:mysql://localhost/l1jdb?useUnicode=true&characterEncoding=utf8");
			DB_LOGIN = serverSettings.getProperty("Login", "root");
			DB_PASSWORD = serverSettings.getProperty("Password", "");
			THREAD_P_TYPE_GENERAL = Integer.parseInt(serverSettings.getProperty("GeneralThreadPoolType", "0"), 10);
			THREAD_P_SIZE_GENERAL = Integer.parseInt(serverSettings.getProperty("GeneralThreadPoolSize", "0"), 10);
			CLIENT_LANGUAGE = Integer.parseInt(serverSettings.getProperty("ClientLanguage", "3"));
			CLIENT_LANGUAGE_CODE = LANGUAGE_CODE_ARRAY[CLIENT_LANGUAGE];
			TIME_ZONE = serverSettings.getProperty("TimeZone", "Asia/Taipei");
			HOSTNAME_LOOKUPS = Boolean.parseBoolean(serverSettings.getProperty("HostnameLookups", "false"));
			AUTOMATIC_KICK = Integer.parseInt(serverSettings.getProperty("AutomaticKick", "10"));
			AUTO_CREATE_ACCOUNTS = Boolean.parseBoolean(serverSettings.getProperty("AutoCreateAccounts", "true"));
			MAX_ONLINE_USERS = Short.parseShort(serverSettings.getProperty("MaximumOnlineUsers", "30"));
			CACHE_MAP_FILES = Boolean.parseBoolean(serverSettings.getProperty("CacheMapFiles", "false"));
			LOAD_V2_MAP_FILES = Boolean.parseBoolean(serverSettings.getProperty("LoadV2MapFiles", "false"));
			CHECK_MOVE_INTERVAL = Boolean.parseBoolean(serverSettings.getProperty("CheckMoveInterval", "false"));
			CHECK_ATTACK_INTERVAL = Boolean.parseBoolean(serverSettings.getProperty("CheckAttackInterval", "false"));
			CHECK_SPELL_INTERVAL = Boolean.parseBoolean(serverSettings.getProperty("CheckSpellInterval", "false"));
			INJUSTICE_COUNT = Short.parseShort(serverSettings.getProperty("InjusticeCount", "10"));
			JUSTICE_COUNT = Integer.parseInt(serverSettings.getProperty("JusticeCount", "4"));
			CHECK_STRICTNESS = Integer.parseInt(serverSettings.getProperty("CheckStrictness", "102"));
			ILLEGAL_SPEEDUP_PUNISHMENT = Integer.parseInt(serverSettings.getProperty("Punishment", "0"));
			AUTOSAVE_INTERVAL = Integer.parseInt(serverSettings.getProperty("AutosaveInterval", "1200"), 10);
			AUTOSAVE_INTERVAL_INVENTORY = Integer.parseInt(serverSettings.getProperty("AutosaveIntervalOfInventory", "300"), 10);
			SKILLTIMER_IMPLTYPE = Integer.parseInt(serverSettings.getProperty("SkillTimerImplType", "1"));
			NPCAI_IMPLTYPE = Integer.parseInt(serverSettings.getProperty("NpcAIImplType", "1"));
			TELNET_SERVER = Boolean.parseBoolean(serverSettings.getProperty("TelnetServer", "false"));
			TELNET_SERVER_PORT = Integer.parseInt(serverSettings.getProperty("TelnetServerPort", "23"));
			PC_RECOGNIZE_RANGE = Integer.parseInt(serverSettings.getProperty("PcRecognizeRange", "20"));
			CHARACTER_CONFIG_IN_SERVER_SIDE = Boolean.parseBoolean(serverSettings.getProperty("CharacterConfigInServerSide", "true"));
			ALLOW_2PC = Boolean.parseBoolean(serverSettings.getProperty("Allow2PC", "true"));
			LEVEL_DOWN_RANGE = Integer.parseInt(serverSettings.getProperty("LevelDownRange", "0"));
			SEND_PACKET_BEFORE_TELEPORT = Boolean.parseBoolean(serverSettings.getProperty("SendPacketBeforeTeleport", "false"));
			DETECT_DB_RESOURCE_LEAKS = Boolean.parseBoolean(serverSettings.getProperty("EnableDatabaseResourceLeaksDetection", "false"));
			CmdActive = Boolean.parseBoolean(serverSettings.getProperty("CmdActive", "false"));
			Announcements_Cycle_Time = Integer.parseInt(serverSettings.getProperty("AnnouncementsCycleTime", "10"));
			Announcements_Cycle_Modify_Time = Boolean.parseBoolean(serverSettings.getProperty("AnnounceTimeDisplay", "True"));
		}
		catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			throw new Error("Failed to Load " + SERVER_CONFIG_FILE + " File.");
		}

		// rates.properties
		try {
			Properties rateSettings = new Properties();
			InputStream is = new FileInputStream(new File(RATES_CONFIG_FILE));
			rateSettings.load(is);
			is.close();

			Rate_CombatExp = Integer.parseInt(rateSettings.getProperty("RateCombatExp", "1"));
			Rate_JobExp = Integer.parseInt(rateSettings.getProperty("RateJobExp", "1"));
			Rate_PetExp = Integer.parseInt(rateSettings.getProperty("RatePetExp", "1"));
			Rate_DollExp = Integer.parseInt(rateSettings.getProperty("RateDollExp", "1"));
			Rate_Honor = Integer.parseInt(rateSettings.getProperty("RateHonor", "1"));
			EleEnchant_Weapon = Integer.parseInt(rateSettings.getProperty("EleEnchantWeapon", "1"));
			EleEnchant_Armor = Integer.parseInt(rateSettings.getProperty("EleEnchantArmor", "1"));
			REWARD_TIME = Integer.parseInt(rateSettings.getProperty("RewardTime", "60"));
			REWARD_COUNT = Integer.parseInt(rateSettings.getProperty("RewardCount", "1"));
			RATE_XP = Double.parseDouble(rateSettings.getProperty("RateXp", "1.0"));
			RATE_LA = Double.parseDouble(rateSettings.getProperty("RateLawful", "1.0"));
			RATE_KARMA = Double.parseDouble(rateSettings.getProperty("RateKarma", "1.0"));
			RATE_DROP_ADENA = Double.parseDouble(rateSettings.getProperty("RateDropAdena", "1.0"));
			RATE_DROP_ITEMS = Double.parseDouble(rateSettings.getProperty("RateDropItems", "1.0"));
			ENCHANT_CHANCE_WEAPON = Integer.parseInt(rateSettings.getProperty("EnchantChanceWeapon", "68"));
			ENCHANT_CHANCE_ARMOR = Integer.parseInt(rateSettings.getProperty("EnchantChanceArmor", "52"));
			ATTR_ENCHANT_CHANCE = Integer.parseInt(rateSettings.getProperty("AttrEnchantChance", "10"));
			RATE_WEIGHT_LIMIT = Double.parseDouble(rateSettings.getProperty("RateWeightLimit", "1"));
			RATE_WEIGHT_LIMIT_PET = Double.parseDouble(rateSettings.getProperty("RateWeightLimitforPet", "1"));
			RATE_SHOP_SELLING_PRICE = Double.parseDouble(rateSettings.getProperty("RateShopSellingPrice", "1.0"));
			RATE_SHOP_PURCHASING_PRICE = Double.parseDouble(rateSettings.getProperty("RateShopPurchasingPrice", "1.0"));
			CREATE_CHANCE_DIARY = Integer.parseInt(rateSettings.getProperty("CreateChanceDiary", "33"));
			CREATE_CHANCE_RECOLLECTION = Integer.parseInt(rateSettings.getProperty("CreateChanceRecollection", "90"));
			CREATE_CHANCE_MYSTERIOUS = Integer.parseInt(rateSettings.getProperty("CreateChanceMysterious", "90"));
			CREATE_CHANCE_PROCESSING = Integer.parseInt(rateSettings.getProperty("CreateChanceProcessing", "90"));
			CREATE_CHANCE_PROCESSING_DIAMOND = Integer.parseInt(rateSettings.getProperty("CreateChanceProcessingDiamond", "90"));
			CREATE_CHANCE_DANTES = Integer.parseInt(rateSettings.getProperty("CreateChanceDantes", "50"));
			CREATE_CHANCE_ANCIENT_AMULET = Integer.parseInt(rateSettings.getProperty("CreateChanceAncientAmulet", "90"));
			CREATE_CHANCE_HISTORY_BOOK = Integer.parseInt(rateSettings.getProperty("CreateChanceHistoryBook", "50"));
			MAGIC_STONE_TYPE = Integer.parseInt(rateSettings.getProperty("MagicStoneAttr", "50"));
			MAGIC_STONE_LEVEL = Integer.parseInt(rateSettings.getProperty("MagicStoneLevel", "50"));
			Ori_Rate_CombatExp = Rate_CombatExp;
			Ori_Rate_JobExp = Rate_JobExp;
			Ori_Rate_PetExp = Rate_PetExp;
			Ori_Rate_DollExp = Rate_DollExp;
			Ori_Rate_Honor = Rate_Honor;
			Ori_EleEnchant_Weapon = EleEnchant_Weapon;
			Ori_EleEnchant_Armor = EleEnchant_Armor;
			Ori_REWARD_TIME = REWARD_TIME;
			Ori_REWARD_COUNT = REWARD_COUNT;
			Ori_REWARD_ITEM = REWARD_ITEM;	
			Ori_RATE_XP = RATE_XP;
			Ori_RATE_LA = RATE_LA;
			Ori_RATE_KARMA = RATE_KARMA;
			Ori_RATE_DROP_ADENA = RATE_DROP_ADENA;
			Ori_RATE_DROP_ITEMS = RATE_DROP_ITEMS;
			Ori_ENCHANT_CHANCE_WEAPON = ENCHANT_CHANCE_WEAPON;
			Ori_ENCHANT_CHANCE_ARMOR = ENCHANT_CHANCE_ARMOR;
		} catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			throw new Error("Failed to Load " + RATES_CONFIG_FILE + " File.");
		}

		// altsettings.properties
		try {
			Properties altSettings = new Properties();
			InputStream is = new FileInputStream(new File(ALT_SETTINGS_FILE));
			altSettings.load(is);
			is.close();

			ONLINE_REWARD = Boolean.parseBoolean(altSettings.getProperty("OnlineReward", "true"));
			REWARD_ITEM = Integer.parseInt(altSettings.getProperty("RewardItem", "0"));
			GLOBAL_CHAT_LEVEL = Short.parseShort(altSettings.getProperty("GlobalChatLevel", "30"));
			WHISPER_CHAT_LEVEL = Short.parseShort(altSettings.getProperty("WhisperChatLevel", "5"));
			AUTO_LOOT = Byte.parseByte(altSettings.getProperty("AutoLoot", "2"));
			LOOTING_RANGE = Integer.parseInt(altSettings.getProperty("LootingRange", "3"));
			ALT_NONPVP = Boolean.parseBoolean(altSettings.getProperty("NonPvP", "true"));
			ALT_ATKMSG = Boolean.parseBoolean(altSettings.getProperty("AttackMessageOn", "true"));
			CHANGE_TITLE_BY_ONESELF = Boolean.parseBoolean(altSettings.getProperty("ChangeTitleByOneself", "false"));
			MAX_CLAN_MEMBER = Integer.parseInt(altSettings.getProperty("MaxClanMember", "0"));
			CLAN_ALLIANCE = Boolean.parseBoolean(altSettings.getProperty("ClanAlliance", "true"));
			MAX_PT = Integer.parseInt(altSettings.getProperty("MaxPT", "8"));
			MAX_CHAT_PT = Integer.parseInt(altSettings.getProperty("MaxChatPT", "8"));
			SIM_WAR_PENALTY = Boolean.parseBoolean(altSettings.getProperty("SimWarPenalty", "true"));
			GUI = Boolean.parseBoolean(altSettings.getProperty("GUI", "true"));//管理者介面byeric1300460
			GET_BACK = Boolean.parseBoolean(altSettings.getProperty("GetBack", "false"));
			ALT_ITEM_DELETION_TYPE = altSettings.getProperty("ItemDeletionType", "auto");
			ALT_ITEM_DELETION_TIME = Integer.parseInt(altSettings.getProperty("ItemDeletionTime", "10"));
			ALT_ITEM_DELETION_RANGE = Integer.parseInt(altSettings.getProperty("ItemDeletionRange", "5"));
			ALT_GMSHOP = Boolean.parseBoolean(altSettings.getProperty("GMshop", "false"));
			ALT_GMSHOP_MIN_ID = Integer.parseInt(altSettings.getProperty("GMshopMinID", "0xffffffff")); // 設定錯誤時就取消GM商店
			ALT_GMSHOP_MAX_ID = Integer.parseInt(altSettings.getProperty("GMshopMaxID", "0xffffffff")); // 設定錯誤時就取消GM商店
			ALT_HALLOWEENIVENT = Boolean.parseBoolean(altSettings.getProperty("HalloweenIvent", "true"));
			ALT_JPPRIVILEGED = Boolean.parseBoolean(altSettings.getProperty("JpPrivileged", "false"));
			ALT_TALKINGSCROLLQUEST = Boolean.parseBoolean(altSettings.getProperty("TalkingScrollQuest", "false"));
			ALT_WHO_COMMAND = Boolean.parseBoolean(altSettings.getProperty("WhoCommand", "false"));
			ALT_REVIVAL_POTION = Boolean.parseBoolean(altSettings.getProperty("RevivalPotion", "false"));
			GDROPITEM_TIME = Integer.parseInt(altSettings.getProperty("GDropItemTime", "10")); 
			CrackStartTime = Short.parseShort(altSettings.getProperty("CrackStartTime", "4"));// 時空裂痕開啟時間(單位小時)
			CrackOpenTime = Short.parseShort(altSettings.getProperty("CrackOpenTime", "60"));// 時空裂痕打開多久(單位分鐘)

			String strWar;
			strWar = altSettings.getProperty("WarTime", "2h");
			if (strWar.indexOf("d") >= 0) {
				ALT_WAR_TIME_UNIT = Calendar.DATE;
				strWar = strWar.replace("d", "");
			}
			else if (strWar.indexOf("h") >= 0) {
				ALT_WAR_TIME_UNIT = Calendar.HOUR_OF_DAY;
				strWar = strWar.replace("h", "");
			}
			else if (strWar.indexOf("m") >= 0) {
				ALT_WAR_TIME_UNIT = Calendar.MINUTE;
				strWar = strWar.replace("m", "");
			}
			ALT_WAR_TIME = Integer.parseInt(strWar);
			strWar = altSettings.getProperty("WarInterval", "4d");
			if (strWar.indexOf("d") >= 0) {
				ALT_WAR_INTERVAL_UNIT = Calendar.DATE;
				strWar = strWar.replace("d", "");
			}
			else if (strWar.indexOf("h") >= 0) {
				ALT_WAR_INTERVAL_UNIT = Calendar.HOUR_OF_DAY;
				strWar = strWar.replace("h", "");
			}
			else if (strWar.indexOf("m") >= 0) {
				ALT_WAR_INTERVAL_UNIT = Calendar.MINUTE;
				strWar = strWar.replace("m", "");
			}
			ALT_WAR_INTERVAL = Integer.parseInt(strWar);
			SPAWN_HOME_POINT = Boolean.parseBoolean(altSettings.getProperty("SpawnHomePoint", "true"));
			SPAWN_HOME_POINT_COUNT = Integer.parseInt(altSettings.getProperty("SpawnHomePointCount", "2"));
			SPAWN_HOME_POINT_DELAY = Integer.parseInt(altSettings.getProperty("SpawnHomePointDelay", "100"));
			SPAWN_HOME_POINT_RANGE = Integer.parseInt(altSettings.getProperty("SpawnHomePointRange", "8"));
			INIT_BOSS_SPAWN = Boolean.parseBoolean(altSettings.getProperty("InitBossSpawn", "true"));
			ELEMENTAL_STONE_AMOUNT = Integer.parseInt(altSettings.getProperty("ElementalStoneAmount", "300"));
			HOUSE_TAX_INTERVAL = Integer.parseInt(altSettings.getProperty("HouseTaxInterval", "10"));
			MAX_DOLL_COUNT = Integer.parseInt(altSettings.getProperty("MaxDollCount", "1"));
			RETURN_TO_NATURE = Boolean.parseBoolean(altSettings.getProperty("ReturnToNature", "false"));
			MAX_NPC_ITEM = Integer.parseInt(altSettings.getProperty("MaxNpcItem", "8"));
			MAX_PERSONAL_WAREHOUSE_ITEM = Integer.parseInt(altSettings.getProperty("MaxPersonalWarehouseItem", "100"));
			MAX_CLAN_WAREHOUSE_ITEM = Integer.parseInt(altSettings.getProperty("MaxClanWarehouseItem", "200"));
			DELETE_CHARACTER_AFTER_7DAYS = Boolean.parseBoolean(altSettings.getProperty("DeleteCharacterAfter7Days", "True"));
			NPC_DELETION_TIME = Integer.parseInt(altSettings.getProperty("NpcDeletionTime", "10"));
			DEFAULT_CHARACTER_SLOT = Integer.parseInt(altSettings.getProperty("DefaultCharacterSlot", "6"));
		}
		catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			throw new Error("Failed to Load " + ALT_SETTINGS_FILE + " File.");
		}

		// charsettings.properties
		try {
			Properties charSettings = new Properties();
			InputStream is = new FileInputStream(new File(CHAR_SETTINGS_CONFIG_FILE));
			charSettings.load(is);
			is.close();

			PRINCE_MAX_HP = Integer.parseInt(charSettings.getProperty("PrinceMaxHP", "1000"));
			PRINCE_MAX_MP = Integer.parseInt(charSettings.getProperty("PrinceMaxMP", "800"));
			KNIGHT_MAX_HP = Integer.parseInt(charSettings.getProperty("KnightMaxHP", "1400"));
			KNIGHT_MAX_MP = Integer.parseInt(charSettings.getProperty("KnightMaxMP", "600"));
			ELF_MAX_HP = Integer.parseInt(charSettings.getProperty("ElfMaxHP", "1000"));
			ELF_MAX_MP = Integer.parseInt(charSettings.getProperty("ElfMaxMP", "900"));
			WIZARD_MAX_HP = Integer.parseInt(charSettings.getProperty("WizardMaxHP", "800"));
			WIZARD_MAX_MP = Integer.parseInt(charSettings.getProperty("WizardMaxMP", "1200"));
			DARKELF_MAX_HP = Integer.parseInt(charSettings.getProperty("DarkelfMaxHP", "1000"));
			DARKELF_MAX_MP = Integer.parseInt(charSettings.getProperty("DarkelfMaxMP", "900"));
			DRAGONKNIGHT_MAX_HP = Integer.parseInt(charSettings.getProperty("DragonKnightMaxHP", "1400"));
			DRAGONKNIGHT_MAX_MP = Integer.parseInt(charSettings.getProperty("DragonKnightMaxMP", "600"));
			ILLUSIONIST_MAX_HP = Integer.parseInt(charSettings.getProperty("IllusionistMaxHP", "900"));
			ILLUSIONIST_MAX_MP = Integer.parseInt(charSettings.getProperty("IllusionistMaxMP", "1100"));
			LV50_EXP = Integer.parseInt(charSettings.getProperty("Lv50Exp", "1"));
			LV51_EXP = Integer.parseInt(charSettings.getProperty("Lv51Exp", "1"));
			LV52_EXP = Integer.parseInt(charSettings.getProperty("Lv52Exp", "1"));
			LV53_EXP = Integer.parseInt(charSettings.getProperty("Lv53Exp", "1"));
			LV54_EXP = Integer.parseInt(charSettings.getProperty("Lv54Exp", "1"));
			LV55_EXP = Integer.parseInt(charSettings.getProperty("Lv55Exp", "1"));
			LV56_EXP = Integer.parseInt(charSettings.getProperty("Lv56Exp", "1"));
			LV57_EXP = Integer.parseInt(charSettings.getProperty("Lv57Exp", "1"));
			LV58_EXP = Integer.parseInt(charSettings.getProperty("Lv58Exp", "1"));
			LV59_EXP = Integer.parseInt(charSettings.getProperty("Lv59Exp", "1"));
			LV60_EXP = Integer.parseInt(charSettings.getProperty("Lv60Exp", "1"));
			LV61_EXP = Integer.parseInt(charSettings.getProperty("Lv61Exp", "1"));
			LV62_EXP = Integer.parseInt(charSettings.getProperty("Lv62Exp", "1"));
			LV63_EXP = Integer.parseInt(charSettings.getProperty("Lv63Exp", "1"));
			LV64_EXP = Integer.parseInt(charSettings.getProperty("Lv64Exp", "1"));
			LV65_EXP = Integer.parseInt(charSettings.getProperty("Lv65Exp", "2"));
			LV66_EXP = Integer.parseInt(charSettings.getProperty("Lv66Exp", "2"));
			LV67_EXP = Integer.parseInt(charSettings.getProperty("Lv67Exp", "2"));
			LV68_EXP = Integer.parseInt(charSettings.getProperty("Lv68Exp", "2"));
			LV69_EXP = Integer.parseInt(charSettings.getProperty("Lv69Exp", "2"));
			LV70_EXP = Integer.parseInt(charSettings.getProperty("Lv70Exp", "4"));
			LV71_EXP = Integer.parseInt(charSettings.getProperty("Lv71Exp", "4"));
			LV72_EXP = Integer.parseInt(charSettings.getProperty("Lv72Exp", "4"));
			LV73_EXP = Integer.parseInt(charSettings.getProperty("Lv73Exp", "4"));
			LV74_EXP = Integer.parseInt(charSettings.getProperty("Lv74Exp", "4"));
			LV75_EXP = Integer.parseInt(charSettings.getProperty("Lv75Exp", "8"));
			LV76_EXP = Integer.parseInt(charSettings.getProperty("Lv76Exp", "8"));
			LV77_EXP = Integer.parseInt(charSettings.getProperty("Lv77Exp", "8"));
			LV78_EXP = Integer.parseInt(charSettings.getProperty("Lv78Exp", "8"));
			LV79_EXP = Integer.parseInt(charSettings.getProperty("Lv79Exp", "8"));
			LV80_EXP = Integer.parseInt(charSettings.getProperty("Lv80Exp", "8"));
			LV81_EXP = Integer.parseInt(charSettings.getProperty("Lv81Exp", "16"));
			LV82_EXP = Integer.parseInt(charSettings.getProperty("Lv82Exp", "16"));
			LV83_EXP = Integer.parseInt(charSettings.getProperty("Lv83Exp", "16"));
			LV84_EXP = Integer.parseInt(charSettings.getProperty("Lv84Exp", "16"));
			LV85_EXP = Integer.parseInt(charSettings.getProperty("Lv85Exp", "16"));
			LV86_EXP = Integer.parseInt(charSettings.getProperty("Lv86Exp", "32"));
			LV87_EXP = Integer.parseInt(charSettings.getProperty("Lv87Exp", "32"));
			LV88_EXP = Integer.parseInt(charSettings.getProperty("Lv88Exp", "32"));
			LV89_EXP = Integer.parseInt(charSettings.getProperty("Lv89Exp", "32"));
			LV90_EXP = Integer.parseInt(charSettings.getProperty("Lv90Exp", "32"));
			LV91_EXP = Integer.parseInt(charSettings.getProperty("Lv91Exp", "64"));
			LV92_EXP = Integer.parseInt(charSettings.getProperty("Lv92Exp", "64"));
			LV93_EXP = Integer.parseInt(charSettings.getProperty("Lv93Exp", "64"));
			LV94_EXP = Integer.parseInt(charSettings.getProperty("Lv94Exp", "64"));
			LV95_EXP = Integer.parseInt(charSettings.getProperty("Lv95Exp", "64"));
			LV96_EXP = Integer.parseInt(charSettings.getProperty("Lv96Exp", "128"));
			LV97_EXP = Integer.parseInt(charSettings.getProperty("Lv97Exp", "128"));
			LV98_EXP = Integer.parseInt(charSettings.getProperty("Lv98Exp", "128"));
			LV99_EXP = Integer.parseInt(charSettings.getProperty("Lv99Exp", "128"));
			LV100_EXP = Integer.parseInt(charSettings.getProperty("Lv100Exp", "128"));
			LV101_EXP = Integer.parseInt(charSettings.getProperty("Lv101Exp", "256"));
			LV102_EXP = Integer.parseInt(charSettings.getProperty("Lv102Exp", "256"));
			LV103_EXP = Integer.parseInt(charSettings.getProperty("Lv103Exp", "256"));
			LV104_EXP = Integer.parseInt(charSettings.getProperty("Lv104Exp", "256"));
			LV105_EXP = Integer.parseInt(charSettings.getProperty("Lv105Exp", "256"));
			LV106_EXP = Integer.parseInt(charSettings.getProperty("Lv106Exp", "512"));
			LV107_EXP = Integer.parseInt(charSettings.getProperty("Lv107Exp", "512"));
			LV108_EXP = Integer.parseInt(charSettings.getProperty("Lv108Exp", "512"));
			LV109_EXP = Integer.parseInt(charSettings.getProperty("Lv109Exp", "512"));
			LV110_EXP = Integer.parseInt(charSettings.getProperty("Lv110Exp", "512"));
			LV111_EXP = Integer.parseInt(charSettings.getProperty("Lv111Exp", "1024"));
			LV112_EXP = Integer.parseInt(charSettings.getProperty("Lv112Exp", "1024"));
			LV113_EXP = Integer.parseInt(charSettings.getProperty("Lv113Exp", "1024"));
			LV114_EXP = Integer.parseInt(charSettings.getProperty("Lv114Exp", "1024"));
			LV115_EXP = Integer.parseInt(charSettings.getProperty("Lv115Exp", "1024"));
			LV116_EXP = Integer.parseInt(charSettings.getProperty("Lv116Exp", "2028"));
			LV117_EXP = Integer.parseInt(charSettings.getProperty("Lv117Exp", "2028"));
			LV118_EXP = Integer.parseInt(charSettings.getProperty("Lv118Exp", "2028"));
			LV119_EXP = Integer.parseInt(charSettings.getProperty("Lv119Exp", "2028"));
			LV120_EXP = Integer.parseInt(charSettings.getProperty("Lv120Exp", "4056"));
		}
		catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			throw new Error("Failed to Load " + CHAR_SETTINGS_CONFIG_FILE + " File.");
		}

		// fights.properties
		Properties fightSettings = new Properties();
		try {
			InputStream is = new FileInputStream(new File(FIGHT_SETTINGS_CONFIG_FILE));
			fightSettings.load(is);
			is.close();
			
			FIGHT_IS_ACTIVE = Boolean.parseBoolean(fightSettings.getProperty("FightIsActive", "False"));
			NOVICE_PROTECTION_IS_ACTIVE = Boolean.parseBoolean(fightSettings.getProperty("NoviceProtectionIsActive", "False"));
			NOVICE_MAX_LEVEL = Integer.parseInt(fightSettings.getProperty("NoviceMaxLevel", "20"));
			NOVICE_PROTECTION_LEVEL_RANGE = Integer.parseInt(fightSettings.getProperty("ProtectionLevelRange", "10"));
		}
		catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			throw new Error("無法讀取設定檔: " + FIGHT_SETTINGS_CONFIG_FILE);
		}
		
		// record.properties
		try {
			Properties recordSettings = new Properties();
			InputStream is = new FileInputStream(new File(RECORD_SETTINGS_CONFIG_FILE));
			recordSettings.load(is);
			is.close();
			
			LOGGING_WEAPON_ENCHANT = Byte.parseByte(recordSettings.getProperty("LoggingWeaponEnchant", "0"));
			LOGGING_ARMOR_ENCHANT = Byte.parseByte(recordSettings.getProperty("LoggingArmorEnchant", "0"));
			LOGGING_CHAT_NORMAL = Boolean.parseBoolean(recordSettings.getProperty("LoggingChatNormal", "false"));
			LOGGING_CHAT_WHISPER = Boolean.parseBoolean(recordSettings.getProperty("LoggingChatWhisper", "false"));
			LOGGING_CHAT_SHOUT = Boolean.parseBoolean(recordSettings.getProperty("LoggingChatShout", "false"));
			LOGGING_CHAT_WORLD = Boolean.parseBoolean(recordSettings.getProperty("LoggingChatWorld", "false"));
			LOGGING_CHAT_CLAN = Boolean.parseBoolean(recordSettings.getProperty("LoggingChatClan", "false"));
			LOGGING_CHAT_PARTY = Boolean.parseBoolean(recordSettings.getProperty("LoggingChatParty", "false"));
			LOGGING_CHAT_COMBINED = Boolean.parseBoolean(recordSettings.getProperty("LoggingChatCombined", "false"));
			LOGGING_CHAT_CHAT_PARTY = Boolean.parseBoolean(recordSettings.getProperty("LoggingChatChatParty", "false"));
			writeTradeLog = Boolean.parseBoolean(recordSettings.getProperty("writeTradeLog", "false"));
			writeRobotsLog = Boolean.parseBoolean(recordSettings.getProperty("writeRobotsLog", "false"));
			writeDropLog = Boolean.parseBoolean(recordSettings.getProperty("writeDropLog", "false"));
			MysqlAutoBackup = Integer.parseInt(recordSettings.getProperty("MysqlAutoBackup", "false"));
			CompressGzip = Boolean.parseBoolean(recordSettings.getProperty("CompressGzip", "false"));
			
		}catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			throw new Error("Failed to Load: " + RECORD_SETTINGS_CONFIG_FILE);
		}

		validate();
	}

	private static void validate() {
		if (!IntRange.includes(Config.ALT_ITEM_DELETION_RANGE, 0, 5)) {
			throw new IllegalStateException("ItemDeletionRange 的設定值超出範圍。");
		}

		if (!IntRange.includes(Config.ALT_ITEM_DELETION_TIME, 1, 35791)) {
			throw new IllegalStateException("ItemDeletionTime 的設定值超出範圍。");
		}
	}

	public static boolean setParameterValue(String pName, String pValue) {
		// server.properties
		if (pName.equalsIgnoreCase("GameserverHostname")) {
			GAME_SERVER_HOST_NAME = pValue;
		}
		else if (pName.equalsIgnoreCase("GameserverPort")) {
			GAME_SERVER_PORT = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Driver")) {
			DB_DRIVER = pValue;
		}
		else if (pName.equalsIgnoreCase("URL")) {
			DB_URL = pValue;
		}
		else if (pName.equalsIgnoreCase("Login")) {
			DB_LOGIN = pValue;
		}
		else if (pName.equalsIgnoreCase("Password")) {
			DB_PASSWORD = pValue;
		}
		else if (pName.equalsIgnoreCase("ClientLanguage")) {
			CLIENT_LANGUAGE = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("TimeZone")) {
			TIME_ZONE = pValue;
		}
		else if (pName.equalsIgnoreCase("AutomaticKick")) {
			AUTOMATIC_KICK = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("AutoCreateAccounts")) {
			AUTO_CREATE_ACCOUNTS = Boolean.parseBoolean(pValue);
		}
		else if (pName.equalsIgnoreCase("MaximumOnlineUsers")) {
			MAX_ONLINE_USERS = Short.parseShort(pValue);
		}
		else if (pName.equalsIgnoreCase("CharacterConfigInServerSide")) {
			CHARACTER_CONFIG_IN_SERVER_SIDE = Boolean.parseBoolean(pValue);
		}
		else if (pName.equalsIgnoreCase("Allow2PC")) {
			ALLOW_2PC = Boolean.parseBoolean(pValue);
		}
		else if (pName.equalsIgnoreCase("LevelDownRange")) {
			LEVEL_DOWN_RANGE = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("SendPacketBeforeTeleport")) {
			SEND_PACKET_BEFORE_TELEPORT = Boolean.parseBoolean(pValue);
		}
		else if (pName.equalsIgnoreCase("Punishment")) {
			ILLEGAL_SPEEDUP_PUNISHMENT = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("AnnounceTimeDisplay")) {
		    Announcements_Cycle_Modify_Time = Boolean.parseBoolean(pValue);
		}
		// rates.properties		
		else if (pName.equalsIgnoreCase("RateCombatExp")) {
			Rate_CombatExp = Integer.parseInt(pValue);
		} 
		else if (pName.equalsIgnoreCase("RateJobExp")) {
			Rate_JobExp = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("RatePetExp")) {
			Rate_PetExp = Integer.parseInt(pValue);
		} 
		else if (pName.equalsIgnoreCase("RateDollExp")) {
			Rate_DollExp = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("RateHonor")) {
			Rate_Honor = Integer.parseInt(pValue);
		} 
		else if (pName.equalsIgnoreCase("EleEnchantWeapon")) {
			EleEnchant_Weapon = Integer.parseInt(pValue);
		} 
		else if (pName.equalsIgnoreCase("EleEnchantArmor")) {
			EleEnchant_Armor = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("RewardTime")) {
			REWARD_TIME = Integer.parseInt(pValue);
		} 
		else if (pName.equalsIgnoreCase("RewardCount")) {
			REWARD_COUNT = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("RateXp")) {
			RATE_XP = Double.parseDouble(pValue);
		}
		else if (pName.equalsIgnoreCase("RateLawful")) {
			RATE_LA = Double.parseDouble(pValue);
		}
		else if (pName.equalsIgnoreCase("RateKarma")) {
			RATE_KARMA = Double.parseDouble(pValue);
		}
		else if (pName.equalsIgnoreCase("RateDropAdena")) {
			RATE_DROP_ADENA = Double.parseDouble(pValue);
		}
		else if (pName.equalsIgnoreCase("RateDropItems")) {
			RATE_DROP_ITEMS = Double.parseDouble(pValue);
		}
		else if (pName.equalsIgnoreCase("EnchantChanceWeapon")) {
			ENCHANT_CHANCE_WEAPON = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("EnchantChanceArmor")) {
			ENCHANT_CHANCE_ARMOR = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("AttrEnchantChance")) {
			ATTR_ENCHANT_CHANCE = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Weightrate")) {
			RATE_WEIGHT_LIMIT = Byte.parseByte(pValue);
		}
		// altsettings.properties
		else if (pName.equalsIgnoreCase("OnlineReward")) {
			ONLINE_REWARD = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("RewardItem")) {
			REWARD_ITEM = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("GlobalChatLevel")) {
			GLOBAL_CHAT_LEVEL = Short.parseShort(pValue);
		}
		else if (pName.equalsIgnoreCase("WhisperChatLevel")) {
			WHISPER_CHAT_LEVEL = Short.parseShort(pValue);
		}
		else if (pName.equalsIgnoreCase("AutoLoot")) {
			AUTO_LOOT = Byte.parseByte(pValue);
		}
		else if (pName.equalsIgnoreCase("LOOTING_RANGE")) {
			LOOTING_RANGE = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("AltNonPvP")) {
			ALT_NONPVP = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("AttackMessageOn")) {
			ALT_ATKMSG = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("ChangeTitleByOneself")) {
			CHANGE_TITLE_BY_ONESELF = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("MaxClanMember")) {
			MAX_CLAN_MEMBER = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("ClanAlliance")) {
			CLAN_ALLIANCE = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("MaxPT")) {
			MAX_PT = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("MaxChatPT")) {
			MAX_CHAT_PT = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("SimWarPenalty")) {
			SIM_WAR_PENALTY = Boolean.valueOf(pValue);
		}
		//管理者介面byeric1300460
        else if (pName.equalsIgnoreCase("GUI")) {
                GUI = Boolean.valueOf(pValue);
        }
        //管理者介面byeric1300460
		else if (pName.equalsIgnoreCase("GetBack")) {
			GET_BACK = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("AutomaticItemDeletionTime")) {
			ALT_ITEM_DELETION_TIME = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("AutomaticItemDeletionRange")) {
			ALT_ITEM_DELETION_RANGE = Byte.parseByte(pValue);
		}
		else if (pName.equalsIgnoreCase("GMshop")) {
			ALT_GMSHOP = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("GMshopMinID")) {
			ALT_GMSHOP_MIN_ID = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("GMshopMaxID")) {
			ALT_GMSHOP_MAX_ID = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("HalloweenIvent")) {
			ALT_HALLOWEENIVENT = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("JpPrivileged")) {
			ALT_JPPRIVILEGED = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("TalkingScrollQuest")) {
			ALT_TALKINGSCROLLQUEST = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("HouseTaxInterval")) {
			HOUSE_TAX_INTERVAL = Integer.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("MaxDollCount")) {
			MAX_DOLL_COUNT = Integer.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("ReturnToNature")) {
			RETURN_TO_NATURE = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("MaxNpcItem")) {
			MAX_NPC_ITEM = Integer.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("MaxPersonalWarehouseItem")) {
			MAX_PERSONAL_WAREHOUSE_ITEM = Integer.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("MaxClanWarehouseItem")) {
			MAX_CLAN_WAREHOUSE_ITEM = Integer.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("DeleteCharacterAfter7Days")) {
			DELETE_CHARACTER_AFTER_7DAYS = Boolean.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("NpcDeletionTime")) {
			NPC_DELETION_TIME = Integer.valueOf(pValue);
		}
		else if (pName.equalsIgnoreCase("DefaultCharacterSlot")) {
			DEFAULT_CHARACTER_SLOT = Integer.valueOf(pValue);
		}
	    else if (pName.equalsIgnoreCase("GDropItemTime")) { 
				GDROPITEM_TIME = Integer.parseInt(pValue);
		}
		// 時空裂痕開啟時間(單位小時)
		else if (pName.equalsIgnoreCase("CrackStartTime")) {
			CrackStartTime = Short.valueOf(pValue);
		}// 時空裂痕開放多久(單位分鐘)
		else if (pName.equalsIgnoreCase("CrackOpenTime")) {
			CrackOpenTime = Short.valueOf(pValue);
		}	
		
		// charsettings.properties
		else if (pName.equalsIgnoreCase("PrinceMaxHP")) {
			PRINCE_MAX_HP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("PrinceMaxMP")) {
			PRINCE_MAX_MP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("KnightMaxHP")) {
			KNIGHT_MAX_HP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("KnightMaxMP")) {
			KNIGHT_MAX_MP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("ElfMaxHP")) {
			ELF_MAX_HP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("ElfMaxMP")) {
			ELF_MAX_MP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("WizardMaxHP")) {
			WIZARD_MAX_HP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("WizardMaxMP")) {
			WIZARD_MAX_MP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("DarkelfMaxHP")) {
			DARKELF_MAX_HP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("DarkelfMaxMP")) {
			DARKELF_MAX_MP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("DragonKnightMaxHP")) {
			DRAGONKNIGHT_MAX_HP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("DragonKnightMaxMP")) {
			DRAGONKNIGHT_MAX_MP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("IllusionistMaxHP")) {
			ILLUSIONIST_MAX_HP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("IllusionistMaxMP")) {
			ILLUSIONIST_MAX_MP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv50Exp")) {
			LV50_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv51Exp")) {
			LV51_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv52Exp")) {
			LV52_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv53Exp")) {
			LV53_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv54Exp")) {
			LV54_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv55Exp")) {
			LV55_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv56Exp")) {
			LV56_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv57Exp")) {
			LV57_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv58Exp")) {
			LV58_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv59Exp")) {
			LV59_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv60Exp")) {
			LV60_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv61Exp")) {
			LV61_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv62Exp")) {
			LV62_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv63Exp")) {
			LV63_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv64Exp")) {
			LV64_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv65Exp")) {
			LV65_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv66Exp")) {
			LV66_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv67Exp")) {
			LV67_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv68Exp")) {
			LV68_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv69Exp")) {
			LV69_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv70Exp")) {
			LV70_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv71Exp")) {
			LV71_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv72Exp")) {
			LV72_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv73Exp")) {
			LV73_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv74Exp")) {
			LV74_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv75Exp")) {
			LV75_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv76Exp")) {
			LV76_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv77Exp")) {
			LV77_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv78Exp")) {
			LV78_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv79Exp")) {
			LV79_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv80Exp")) {
			LV80_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv81Exp")) {
			LV81_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv82Exp")) {
			LV82_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv83Exp")) {
			LV83_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv84Exp")) {
			LV84_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv85Exp")) {
			LV85_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv86Exp")) {
			LV86_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv87Exp")) {
			LV87_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv88Exp")) {
			LV88_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv89Exp")) {
			LV89_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv90Exp")) {
			LV90_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv91Exp")) {
			LV91_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv92Exp")) {
			LV92_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv93Exp")) {
			LV93_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv94Exp")) {
			LV94_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv95Exp")) {
			LV95_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv96Exp")) {
			LV96_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv97Exp")) {
			LV97_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv98Exp")) {
			LV98_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv99Exp")) {
			LV99_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv100Exp")) {
			LV100_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv101Exp")) {
			LV101_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv102Exp")) {
			LV102_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv103Exp")) {
			LV103_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv104Exp")) {
			LV104_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv105Exp")) {
			LV105_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv106Exp")) {
			LV106_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv107Exp")) {
			LV107_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv108Exp")) {
			LV108_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv109Exp")) {
			LV109_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv110Exp")) {
			LV110_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv111Exp")) {
			LV111_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv112Exp")) {
			LV112_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv113Exp")) {
			LV113_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv114Exp")) {
			LV114_EXP = Integer.parseInt(pValue);
		}
		else if (pName.equalsIgnoreCase("Lv115Exp")) {
			LV115_EXP = Integer.parseInt(pValue);
		}
	
		//record.properties
		else if (pName.equalsIgnoreCase("LoggingWeaponEnchant")) {
			LOGGING_WEAPON_ENCHANT = Byte.parseByte(pValue);
		}
		else if (pName.equalsIgnoreCase("LoggingArmorEnchant")) {
			LOGGING_ARMOR_ENCHANT = Byte.parseByte(pValue);
		} else {
			return false;
		}
		return true;
	}

	private Config() {
	}

	public static void updateRates() {

	}
	
	public static void reloadRates() {

	}

	public static void reset() {
		Rate_CombatExp = Ori_Rate_CombatExp;
		Rate_JobExp = Ori_Rate_JobExp;
		Rate_PetExp = Ori_Rate_PetExp;
		Rate_DollExp = Ori_Rate_DollExp;
		Rate_Honor = Ori_Rate_Honor;
		EleEnchant_Weapon = Ori_EleEnchant_Weapon;
		EleEnchant_Armor = Ori_EleEnchant_Armor;
		REWARD_TIME = Ori_REWARD_TIME;
		REWARD_COUNT = Ori_REWARD_COUNT;
		REWARD_ITEM = Ori_REWARD_ITEM;
		RATE_XP = Ori_RATE_XP;
		RATE_LA = Ori_RATE_LA;
		RATE_KARMA = Ori_RATE_KARMA;
		RATE_DROP_ADENA = Ori_RATE_DROP_ADENA;
		RATE_DROP_ITEMS = Ori_RATE_DROP_ITEMS;
		ENCHANT_CHANCE_WEAPON = Ori_ENCHANT_CHANCE_WEAPON;
		ENCHANT_CHANCE_ARMOR = Ori_ENCHANT_CHANCE_ARMOR;
	}
}